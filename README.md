# AT Internet Piano Analytics for Domains

* This module allows configuration of AT Internet Piano Analytics
  on a multi-domains Drupal website.

## Configuration

Domain AT Internet Piano Analytics module have a configuration setting page
to define configuration for each domain.

### Configuration page path
`/admin/config/domain/pianoanalytics`

## Dependencies

* domain
* pianoanalytics
