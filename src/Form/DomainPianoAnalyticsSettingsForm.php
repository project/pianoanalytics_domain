<?php

namespace Drupal\pianoanalytics_domain\Form;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\pianoanalytics\Form\PianoAnalyticsSettingsForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RequestContext;

/**
 * Class DomainPianoAnalyticsSettingsForm.
 *
 * @package Drupal\pianoanalytics_domain\Form
 */
class DomainPianoAnalyticsSettingsForm extends PianoAnalyticsSettingsForm {

  const pianoanalytics_DOMAIN_SETTINGS = 'pianoanalytics_domain.settings';

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected RequestContext $requestContext;

  /**
   * Constructs a DomainPianoAnalyticsSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   The library discovery service.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, FileRepositoryInterface $file_repository, LibraryDiscoveryInterface $library_discovery, RequestContext $request_context) {
    parent::__construct($config_factory, $entity_type_manager, $file_system, $file_repository, $library_discovery);
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('library.discovery'),
      $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $domain_id = $this->getRequest()->get('domain_id');
    return [
      self::pianoanalytics_DOMAIN_SETTINGS . '.' . $domain_id,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pianoanalytics_domain_settings_form';
  }

  /**
   * Get configuration.
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected function getConfig() {
    $domain_id = $this->getRequest()->get('domain_id');
    return $this->config(
      self::pianoanalytics_DOMAIN_SETTINGS . '.' . $domain_id);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $domain_id = $this->getRequest()->get('domain_id');

    $form['domain'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('Domain: @domain_id', ['@domain_id' => $domain_id]),
      '#weight' => -100,
    ];

    return parent::buildForm($form, $form_state);
  }

  protected function getSDKInitFilename() {
    $domain_id = $this->getRequest()->get('domain_id');
    return 'pianoanalytics-sdk-init.' . $domain_id . '.js';
  }

  protected function getPAScriptFilename() {
    $domain_id = $this->getRequest()->get('domain_id');
    return 'pianoanalytics.' . $domain_id . '.js';
  }

  protected function getPALibrary($mode) {
    $domain_id = $this->getRequest()->get('domain_id');
    switch ($mode) {
      case 'generic':
        $library = 'pianoanalytics_domain/pianoanalytics_js.' . $domain_id;
        break;
      case 'url':
        $library = 'pianoanalytics_domain/pianoanalytics_url.' . $domain_id;
        break;
      case 'file':
        $library = 'pianoanalytics_domain/pianoanalytics_file.' . $domain_id;
        break;
      default:
        $library = '';
    }
    return $library;
  }

}
