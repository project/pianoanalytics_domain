<?php

namespace Drupal\pianoanalytics_domain\Configuration;

use Drupal;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class DomainConfigOverride.
 *
 * @package Drupal\pianoanalytics_domain
 */
class DomainConfigOverride implements ConfigFactoryOverrideInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a DomainConfigOverride object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The module handler service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Returns config overrides.
   *
   * @param array $names
   *   A list of configuration names that are being loaded.
   *
   * @return array
   *   An array keyed by configuration name of override data. Override data
   *   contains a nested array structure of overrides.
   */
  public function loadOverrides($names = []) {
    $overrides = [];
    if (in_array('pianoanalytics.settings', $names)) {
      // Get the domain negotiator. Note that injecting the domain negotiator
      // into the service created a circular dependency error, so we load from
      // the core service manager.
      /** @var \Drupal\domain\DomainNegotiator $negotiator */
      $negotiator = Drupal::service('domain.negotiator');
      $domain = $negotiator->getActiveDomain();
      if (!empty($domain)) {
        $domain_key = $domain->id();
        $settings = $this->configFactory->get(
          'pianoanalytics_domain.settings.' . $domain_key);
        // Override the default AT Internet Piano Analytics configuration.
        $overrides['pianoanalytics.settings'] = $settings->getRawData();
      }
    }
    return $overrides;
  }

  /**
   * The string to append to the configuration static cache name.
   *
   * @return string
   *   A string to append to the configuration static cache name.
   */
  public function getCacheSuffix() {
    return 'DomainPianoAnalyticsConfigOverrider';
  }

  /**
   * Gets the cacheability metadata associated with the config factory override.
   *
   * @param string $name
   *   The name of the configuration override to get metadata for.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   A cacheable metadata object.
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * Creates a configuration object for use during install and synchronization.
   *
   * If the overrider stores its overrides in configuration collections then
   * it can have its own implementation of
   * \Drupal\Core\Config\StorableConfigBase. Configuration overriders can link
   * themselves to a configuration collection by listening to the
   * \Drupal\Core\Config\ConfigEvents::COLLECTION_INFO event and adding the
   * collections they are responsible for. Doing this will allow installation
   * and synchronization to use the overrider's implementation of
   * StorableConfigBase.
   *
   * @see \Drupal\Core\Config\ConfigCollectionInfo
   * @see \Drupal\Core\Config\ConfigImporter::importConfig()
   * @see \Drupal\Core\Config\ConfigInstaller::createConfiguration()
   *
   * @param string $name
   *   The configuration object name.
   * @param string $collection
   *   The configuration collection.
   *
   * @return \Drupal\Core\Config\StorableConfigBase
   *   The configuration object for the provided name and collection.
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
